<div align="center">

<img src="https://user-images.githubusercontent.com/65299153/135719227-52adf365-d619-4b1f-b5fe-8aa2bbd25c56.png" width="60" />

<h1 align="center">Elementary DWM</h1>
<p align="center">A pre-patched dwm build complementing for the basic functionalities lacking in vanilla dwm.</p>

</div>

<br>

## What is this and what this isn't ?


[DWM](https://dwm.suckless.org/) is lightweight and blazing fast tiling window manager, however out of the box it lacks a lot of functionality when compared to other modern window managers like i3wm or bspwm.

This lack in functionality can be easily resolved by manually applying [patches](https://dwm.suckless.org/patches/) to its source code. As you might have guessed it, applying patches manually is what keeps away most of DWM's potential userbase.

*ENTER ELEMENTARY DWM*

Elementary DWM aims to provide those **absolutely required functionalities** lacking in dwm like *reloading the wm, rotating the stack elements, cycling through layouts, window gaps and much more*, so that the user can have a smooth experince out of the box. 

Patching DWM is also very tricky. You must deleberality choose your patches as one patch might break another patch, and using all patches for dwm will be an absolute mess.

Elementary DWM uses a list of **curated and tested patches** which should *work harmoniously* with each other.
To keep everything working fine the patches are *primarily focused* on **functionality** rather than being on looks.
This does reduce flexibility but once the users are used to dwm they can easily add there own patches and there are also other projects like [DWM flexipatch](https://github.com/bakkeby/dwm-flexipatch) which gives users the absolute control over their patchs.

Elementary DWM also aims to stick as close to vanilla dwm as possible, hence external programs for showing status info, etc will  not be provided in this build.
 
## Who is this for ?

This project is *mainly geared* towards **intermediate linux** users and those who have **used a tiling-wm before**.
The set of patches provided try to mimmic the functionalities & keybindings of i3wm for ease of use.

TIPS :

* If you are *absolutely new* to linux this is **NOT** for you, it will only make your experince worse.

* *First time* trying out a tiling wm? Try out **i3wm** first then come back here.

* Still persist on using dwm to flex over others at unixporn ? **Try** using *Arcolinux's dwm iso* first and get used to the workflow.

> These tips are only meant for you have a good day. Ofc feel free to not give a **** and dive in for some cheap thrills.

<br>

## Installation

### Setting up dependencies

These packages are **essential** to build DWM.They will provide libraries for DWM to be built and function properly.

> Build dependencies
* git
* development-packages (distribution specific)
* libX11 			
* libXft 			
* libXinerama 

> Elementary-DWM specific dependencies
* xsetroot
* simple-terminal

___

<details><summary>Install on Arch based systems.</summary>

```
sudo pacman -S git base-devel libx11 libxft libxinerama xorg-xsetroot
```
</details>


<details><summary>Install on Debian/Ubuntu based systems.</summary>

```
sudo apt install git build-essential libx11-dev libxft-dev libxinerama-dev xorg-xserver
```
</details>

<details><summary>Install on Fedora based systems.</summary>

```
sudo dnf groupinstall "Development Tools" "Development Libraries"
sudo dnf install git libX11 libxft libxinerama xsetroot
```
</details>

<details><summary>Install on Void.</summary>

```
sudo xbps-install git base-devel libX11 libX11-devel libXft libXft-devel libXinerama libXinerama-devel xsetroot
```
</details>

> It is recommended to install simple terminal from [source](https://st.suckless.org/), you may also install it manually via your package manager.

### Downloading Elementary DWM

Once dependencies are install you are good to download Elementary-DWM.
```
git clone git@gitlab.com:CTZxVULKAN/elementary-dwm.git
```


### Building Elementary DWM

Now build dwm form the downloaded release.
* Copy dwm folder under home folder.
```
cd elementary-dwm
cp -r .dwm ~/
```
> This is **absolutely needed** as autostart patch looks for `autostart.sh` under ~/.dwm/

* Allow user to edit dwm files.
```
sudo chown -R $USER ~/.dwm
```
> Usually dwm files are under root ownership we dont need it to have such previlliges.

* Make sure all scripts required by dwm are exectable
```
cd ~/.dwm
chmod +x autostart.sh
chmod +x scripts/*
```
> Scripts need to be executable to run.

* Make a desktop entry for DWM with the following contents

```
sudo touch /usr/share/xsessions/dwm.desktop
```

```
[Desktop Entry]
Encoding=UTF-8
Name=Dwm
Comment=Dynamic window manager
Exec=/usr/local/bin/dwm
Icon=
Type=XSession
```
> You need to have this entry for your display manager to detect DWM and log you in.

* Build DWM

```
cd ~/.dwm
sudo make clean install
```
> Every time you make a change in `config.h` you will have to build dwm once again for the changes to apply.


## This is good but how do I remove a patch?

All patches in Elementary DWM are added as commits. To remove a patch you will need git and the patch's commit SHA.
A **list** of *patches and commits* can be found [below](#list-of-patches-applied)
* To remove a patch first get a clean version of Elementary-DWM :
```
git clone git@gitlab.com:CTZxVULKAN/elementary-dwm.git
```

* Now remove the patch with its commit SHA
```
cd elementary-dwm
git revert <COMMIT-SHA>

Eg : git revert XXXXXXXXXX
```
> This example *removes* the patch which has a **Commit SHA of XXXXXXXXXX**.

* Finally replace the old version with the clean version and rebuild.
```
rm -rf ~/.dwm
cp -r elementary-dwm/.dwm ~/
cd ~/.dwm
sudo make clean install
```

<br>

## List of Patches applied

<div align="center">

| PATCH | COMMIT SHA | DESCRIPTION |
| ------ | ------ | ------|
| restart-sig        | `c4b4ef315bfac218b79ce77ae63ee6b9f4355e1d` | Allows reloading of dwm with a keybinding. 
| autostart          | `6ac9471cec919752108a117b601192cdaaf33d7c` | Runs specified programs of startup.        
| rotate-stack       | `7bcbdd32462fad42662be7e169e292d77adbb7fd` | Allows changing of window's position in a stack.
| cycle-layouts      | `d04d7dba0b0da78ef13c02b807c3046800459352` | Cycle through all layouts with a single bind.
| always-center      | `0e5cdee886c8e2391cf19e2517c42ec8357f8066` | Always launch floating windows in the center.
| moveresize         | `533c4e91ef2e2709012dde37fce5df8e4b328d42` | Allows to move/resize floating windows.
| save-floats        | `6848b5ed6373670705bbf0c7c3deaa3ee33512b6`| Restores the last dimensions of a floating window.
| actual-fullscreen  | `4fa6480e4b082ea5ea893dcbd97eaadc15879fdb` | Toggle app fullscreen with a bindings.

</div>

> All patches were taken from the [official dwm patch](https://dwm.suckless.org/patches/) site. A local copy of the patches used are provided under `.dwm/patches/`
