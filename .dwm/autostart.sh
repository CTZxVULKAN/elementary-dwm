# add all commands you want to execute on dwm startup
# commands you might want to add here are xrandr to set display res/refresh rate, sxhkd to load keybindings, udiskie to automount disks, nitrogen/feh to set wallpaper,  etc.

$HOME/.dwm/scripts/dwmbar &  # show status info in the bar

